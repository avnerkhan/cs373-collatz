# CS373: Software Engineering Collatz Repo

# Name
Avner Khan
# EID
aak2699 
# GitLab ID
avnerkhan
# HackerRank ID
avnerkhan
# Git SHA
ec44260a9b966aea07cd8e0a2fcca8ee82a76aa3
# link to GitLab pipelines
https://gitlab.com/avnerkhan/cs373-collatz/pipelines
# estimated completion time (hours: int)
10 hours
# actual completion time (hours: int)
12-14 hours
# comments 
No comments